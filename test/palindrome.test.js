const { palindrome } = require('../utils/utility-for-testing')
describe('metodos de Util', () => {
  
  test('palindrome of maximiliano', () => {
    const result = palindrome('maximiliano')
    expect(result).toBe('onailimixam')
  })

  test('palindrome of empty string', () => {
    const result = palindrome('')
    expect(result).toBe('')
  })

  test('palindrome of undefined', () => {
    const result = palindrome()
    expect(result).toBeUndefined()
  })

})
