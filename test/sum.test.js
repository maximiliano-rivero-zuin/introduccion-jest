const sum = require('../utils/sum')

test('add 1 + 2 equals 3', () => {
    expect(sum(1,2)).toBe(3)
})

test('add 5 + 5 equals 10', () => {
    expect(sum(5,5)).toBe(10)
})
